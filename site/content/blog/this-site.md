---
categories: ["posts"]
tags: []
title: "How this website is hosted"
date: 2021-03-08T19:03:43Z
draft: true
slug: "this-site"
---

A lot has gone into this site. Not because it's a particularly complex task, but because it's been a learning experience.  
There's layers of infrastructure. Containers, Container Orchestration, CI and CD, Helm Charts and more.

This post documents a rough outline of how it's hosted, the tools, technologies, and processes.

## The website itself

I use [hugo](https://gohugo.io/) to publish this site.  
There's a wealth of themes available for hugo, and this site leverages [anatole](https://github.com/lxndrblz/anatole/).  
Gone are the days when I laboured over the Holy Trinity (HTML, CSS, & JavaScript). 

Hugo is one of the fastest static site generators (no, [really fast](https://forestry.io/blog/hugo-vs-jekyll-benchmark/)),
which leads to an incredible feedback loop. You can generate a hugo site and service it in an nginx container in under the time it takes
to navigate to your browser and type in the address (and 80% of that time is the Docker engine!).

```bash
➜  stewartplatt git:(master) ✗ time bash -c 'docker build -t shteou/stewartplatt . && docker run -it --rm -d -p 8080:80 shteou/stewartplatt'
Sending build context to Docker daemon  7.776MB
Step 1/6 : FROM klakegg/hugo:0.81.0-busybox as builder
 ---> 40c6ea715711
Step 2/6 : COPY site /site
 ---> bc3c4063e03f
Step 3/6 : WORKDIR /site
 ---> Running in a8ead3db240d
Removing intermediate container a8ead3db240d
 ---> 03f247ace639
Step 4/6 : RUN hugo
 ---> Running in ca6d3ec3fe53
Start building sites …

                   | EN
-------------------+-----
  Pages            | 23
  Paginator pages  |  0
  Non-page files   |  0
  Static files     |  1
  Processed images |  0
  Aliases          |  1
  Sitemaps         |  1
  Cleaned          |  0

Total in 57 ms
Removing intermediate container ca6d3ec3fe53
 ---> 2db2db831c95
Step 5/6 : FROM nginx:1.17-alpine as serve
 ---> 89ec9da68213
Step 6/6 : COPY --from=builder /site/public /usr/share/nginx/html
 ---> Using cache
 ---> 09db2ba94d40
Successfully built 09db2ba94d40
Successfully tagged shteou/stewartplatt:latest
7e77153afaa950bf99aa29273b630022f701f40b52e0134c28271aeacc055e39
bash -c   0.38s user 0.20s system 27% cpu 2.123 total
```

Fast feed back loops are generally under-appreciated, but they can do wonders for your productivity.  
The flip side of this is you lose this excuse.

![XKCD Code Is Compiling](https://imgs.xkcd.com/comics/compiling.png)


## OCI image and chart

As you can see from above, the website is containerised. A [multi-stage](https://docs.docker.com/develop/develop-images/multistage-build/) `Dockerfile`
lets me build and run in a reproducible way, even if I often use `hugo` natively on Linux, Mac, and Windows.

```bash
docker build -t shteou/stewartplatt . && docker run -it --rm -p 8080:8080 shteou/stewartplatt
```

With the [OCI image](https://www.padok.fr/en/blog/container-docker-oci) ready to deploy, we wrap it in a [Helm chart](https://helm.sh/).  
This bundles up the Kubernetes configuration - A `Deployment` and `Service` - ready for deployment to multiple environments.  

## Infrastructure

The Kubernetes cluster is hosted by [Scaleway Elements](https://www.scaleway.com/en/elements/).

It's a cheap way to run a multi-node cluster, with two medium developer instances (clocking in at an impressive €0.04/hr plus tax!).  
The control plane is free, block storage is competitive and even load balancers are affordable.

The Kubernetes clusters run a Traefik ingress controller.

## Future Work

There's quite a lot already, but I'll likely keep plugging away at this, drilling deeper and deeper into a resilient, fully automated setup.

The Helm charts could be made more [secure](https://kubesec.io/) and [elastic](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/).  
There's some CI/CD to layer on top (with GitLab), which could govern the whole build, test and deploy logic (for infrastructure and applications!).  
Terraform needs to be written to provision the Kubernetes Kapsules, DNS and LoadBalancers. That Terraform should be capable of deploying to multiple regions in the case of an outage at Scaleway.  
