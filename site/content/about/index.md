---
comments: false
date: "2016-10-02T22:55:05-04:00"
draft: false
slug: "about"
title: "Curriculum Vitae"

showpagemeta: false
---

## About

> A seasoned, engineer and team lead.  
> A wide range of experience, with a recent focus on building internal developer platforms
> and affecting cultural change in Financial Services

## Skills

* 12 years of professional experience in software engineering
* A polyglot programmer (See [languages](about/me/#languages))
* Experienced with DevSecOps and Cloud Native patterns and practices
* 5 years of experience building internal developer platforms
* A background and continuing interest in security (and much more)

## Work history

```
Currencycloud (2020 - Now) Tech Lead - Cloud Platform Engineering
```

I helped to grow the team from 2 members to 5 as we supported a larger tech
organisation and increased our responsibilities.

We focussed on building an internal developer platform, cementing the guarantees
and constraints of our platform for a large number of developer teams.

It was decided that the Jenkins X platform wasn't a strategic option for us, and so
we looked to replace Jenkins X, eventually choosing Harness CI.

&nbsp;

```
Currencycloud (2019 - 2020) Software Engineer - Cloud Platform Engineering
```

Working to adopt and improve cloud native practices across the tech department.

Lending expertise on microservices, cloud native development, testing in a CD pipeline, and application security.
Facilitating the migration of a small estate of microservices from EC2 instaces to containerised workloads in Kubernetes.

Our team took ownership of much of our CI/CD toolset, based on Jenkins X, and began to expand its capabilities.  

&nbsp;

```
FINkit (2016 - 2019) Test Lead
```

I moved to FinKit as it was a small team Test lead working across multiple teams, working toward a next generation banking platform.
We aim to accelerate financial institutions with continuous deployment, modern architectures, baked in governence and compliance.

FINkit was recently acquired by Fiserv, a leading American provider of financial services platforms.

&nbsp;

```
Nagra Kudelski (2013 - 2016) Senior Test Engineer
```

I joined Nagra where I was embedded in a scrum team to improve testing of their MediaLive JavaScript API.  
There I wrote a karma-compatible test framework to replace a legacy headless approach. Due to some poor
practices baked into the API contract, the framework was built to support isolated execution environments
per test/suite by executing test contexts within iframes.

Shortly after I took over a colleague's responsibilities for testing of Nagra's white label UI.  
The approach was Cucumber BDD tests for a WebDriver backend. I extended and refactored the test suite for
more comprehensive browser and functional coverage, as well as moving toward a cleaner business language
when describing the tests.

The two teams merged, and we went on to hire two junior QAs that I mentored.

Although limited in its scope, it was here that I was first able to break down the "over the wall" mentality toward QA
and to help contribute to my team in shifting toward (what is now widely known as) a DevSecOps culture.

* Built and maintained a test framework for Nagra's JavaScript MediaLive APIs.  
* Took responsibility for a Ruby test framework for Nagra's whitelabel
* Early adopter and evangelist for several initiatives within the company, helping to improve and
  spread awareness of these tools (e.g. gerrit, continuous integration and delivery)
* Evangelised and demonstrated confidence in refactoring through test driven development. 

&nbsp;

```
GoCompare (2011 - 2013) Test Analyst
```

After moving back home to South Wales I joined GoCompare as a test analyst.  
I found myself supporting our marketing team with their efforts to move to a new CMS, in addition to performing a range of QA functions for the project.

I led the company's first efforts in automated system testing, using the emerging WebDriver tool chain.

I was involved with all aspects of QA at GoCompare, as well as contributing to the resolution of high priority incidents

&nbsp;

```
Sophos (2008 - 2011) Lead Test Engineer
```

At Sophos I was the lead test engineer for the company's Cloud based Unified Threat Management (UTM) offering.  
The software was built for a variety of devices, from embedded Linux routers to real time operating systems, on x86, ARM, and MIPS.  
I helped to build, market, and sell the software to medium and large cap networking vendors.

I also worked with the core engine team to deliver and improve QA for the virus engine, anti-virus suites, and Data Leakage Prevention products.

&nbsp;

```
Charles XII (2008) Barman
```

A barman for a busy student pub in Heslington, York.  
The high point of my career, it can only go downhill from here.

## Education

```
University Of York (2005 - 2008) Computer Science
```

I studied computer science at the University of York, graduating with honours.  
My dissertation was entitled "A JIT Compiler for YHC Bytecode" which explored
implementing a fast throwaway Just In Time compiler for the York Haskell Compiler.

The simple and aging technique for JIT compilation showed impressive performance
whilst adding only a few KiB of binary size and only constant memory overhead.  
Had I taken the project further, it would have been interesting to evaluate its
usage in memory constrained environments.


## <a id="languages" name="languages"></a> Languages

Proficient

* C
* C++
* Java
* Javascript
* Python
* Shell scripting

Familiar

* Erlang
* Haskell
* Perl
* Ruby
* Scala
