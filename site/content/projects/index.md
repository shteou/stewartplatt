+++
categories = ["projects"]
comments = false
date = "2016-10-02T22:55:05-04:00"
draft = false
slug = "projects"
tags = ["projects"]
title = "Projects"

showpagemeta = false
+++

## Projects

### [go-ignore](https://github.com/shteou/go-ignore)

A simple library for parsing `.gitignore` and `.dockerignore` files. This library does not concern itself
with evauating ignore rules, it is used in [Conftest](https://github.com/open-policy-agent/conftest) to allow
policies to be written against these files. e.g. to ensure that the `.git` directory is present in `.dockerignore`.

### [LibOfLegends](https://github.com/epicvrvs/LibOfLegends)

LibOfLegends is a C# client for the protocol used by the Adobe AIR based League of Legends client.

In 2012, a friend of mine showed an interest in storing and analysing his stats, so I got to reverse engineering.  
A little while later I had the first few features for LibOfLegends, allowing [epivrvs](https://github.com/epicvrvs) to build
the first version of his stats scraping client [Riot Control](https://github.com/epicvrvs/riotcontrol)

I contributed this code under the pseudonym [OffsetDonor](https://github.com/OffsetDonor).

### [FluorineFXMods](https://github.com/epicvrvs/FluorineFXMods)

FluorineFXMods are a collection of (now ancient) improvements to the popular FluorineFX library
for interacting with RTMP (Real Time Messaging Protocol) applications.

This project added support for RTMPS (RTMP over TLS) and injecting a proxy, for use in the LibOfLegends
project.

## Contributions

### [Conftest](https://github.com/open-policy-agent/conftest)

I was able to make [a few modest contributions](https://github.com/open-policy-agent/conftest/commits?author=shteou)
to Open Policy Agent's Conftest after exploring it for use at [Currencycloud](https://www.currencycloud.com).
